package main

import (
	// "encoding/json"
	"bufio"
    "fmt"
    "os"
	"strconv"
	"log"
)


func main() {

	var bucket = [12]int{0,0,0,0,0,0,0,0,0,0,0,0}
	var bucket2 = [12]int{0,0,0,0,0,0,0,0,0,0,0,0}

	
	f, _ := os.Open("bits.txt")
	if (f != nil) {
		fmt.Println("File opened")
	}
	scanner := bufio.NewScanner(f)
    // Loop over all lines in the file and print them.
    for scanner.Scan() {
      line := scanner.Text()
	  for i, s := range line {
		  if s == 49 {
			  bucket[i] = bucket[i] + 1
		  } else {
			  bucket2[i] = bucket2[i] + 1
		  }
	  }	
	}	
	res := ""
	res2 := ""
	for i, v := range bucket {
		if v > bucket2[i] {
			res = res + "1"
		} else {
			res = res + "0"
		}
		if v < bucket2[i] {	
			res2 = res2 + "1"
		} else {
			res2 = res2 + "0"
		}
	}
	gamma, err := strconv.ParseInt(res, 2, 64)
	if err != nil {
		log.Fatal(err)
	}
	epsilon, err := strconv.ParseInt(res2, 2, 64)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(res)
	fmt.Println(res2)
	fmt.Println(gamma)
	fmt.Println(epsilon)
	fmt.Println(gamma * epsilon)
}
