package main

import (
	// "encoding/json"
	"bufio"
    "fmt"
    "os"
	// "container/list"
	"strconv"
	"log"
	// "golang.org/x/image/vector"
)


func main() {
	
	var oxygen = []string{}
	var co2 = []string{}
	f, _ := os.Open("bits.txt")
	if (f != nil) {
		fmt.Println("File opened")
	}
	scanner := bufio.NewScanner(f)
    // Loop over all lines in the file and print them.
    for scanner.Scan() {
      line := scanner.Text()
	  oxygen = append(oxygen, line)
	  co2 = append(co2, line)
	}	
	compute(oxygen, 0)
	compute2(co2, 0)
}


func compute(arr []string, bit_position int) {
	var one  int32
	var zero int32
	one=0 
	zero=0
	var result = []string{}
	// find more common 1 or 0 in bit position
	for i :=0; i < len(arr); i++ {
		tmp := arr[i]
		// fmt.Println(tmp)
		first_bit := tmp[bit_position]
		// fmt.Println(first_bit)
		if (first_bit == 49) {
			one++
		} else {
			zero++
		}
	}
	// check whether 1 or 0 was more common
	flag := 0
	if (one == zero) {
		fmt.Println("bruh")
		flag = 49
	} else if (one > zero) {
		flag = 49
	} else {
		flag = 48
	}
	for i:=0; i < len(arr); i++ {
		if (arr[i][bit_position] == byte(flag)) {
			result = append(result, arr[i])
		}
	}
	fmt.Println(result)
	fmt.Println("len", len(result), "bit_position", bit_position)
	fmt.Println("\n")
	if (bit_position != len(arr[0])-1) {
		bit_position++
		compute(result, bit_position)
	} else {
		oxy, err := strconv.ParseInt(result[0], 2, 64)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(oxy)
	}
	
}

func compute2(arr []string, bit_position int) {
	var one  int32
	var zero int32
	one=0 
	zero=0
	var result = []string{}
	// find more common 1 or 0 in bit position
	for i :=0; i < len(arr); i++ {
		tmp := arr[i]
		// fmt.Println(tmp)
		first_bit := tmp[bit_position]
		// fmt.Println(first_bit)
		if (first_bit == 49) {
			one++
		} else {
			zero++
		}
	}
	// check whether 1 or 0 was more common
	flag := 0
	if (one == zero) {
		flag = 48
	} else if (one > zero) {
		flag = 48
	} else {
		flag = 49
	}
	for i:=0; i < len(arr); i++ {
		if (arr[i][bit_position] == byte(flag)) {
			result = append(result, arr[i])
		}
	}
	fmt.Println("len", len(result), "bit_position", bit_position)
	fmt.Println(result)
	fmt.Println("\n")
	if (bit_position != len(arr[0])-1) {
		bit_position++
		compute(result, bit_position)
	} else {
		c0, err := strconv.ParseInt(result[0], 2, 64)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(c0)
	}
	
	
}
